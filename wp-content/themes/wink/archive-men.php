<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress Kickstarter Theme
 */

get_header(); ?>
	<div class="container">
	<section class="primary">
		<div class="entry" role="main">
			<?php if( have_posts() ): ?>
				
				<div class="breadcrumbs">
			    <?php if(function_exists('bcn_display'))
			    {
			      bcn_display();
			    }?>
			  </div>
			  				
				<?php					
					while( have_posts() ): the_post();						
						get_template_part( "templates/partials/inc", "content" );
					endwhile; 
					thg_load_woopagination( true );
					 
				?>
				
			<?php else: ?>
				
				<?php get_template_part( 'templates/partials/inc', 'noresult' ); ?>
				
			<?php endif; ?>
		</div><!-- END .entry -->
	</section><!-- END .primary -->
	
	<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>