<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WordPress Kickstarter Theme
 */

global $woo_options; 

?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php 
		// Print the <title> tag based on what is being viewed.
		global $page, $paged;
		wp_title( '|', true, 'right' );
		// Add the blog name.
		//bloginfo( 'name' );
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', get_option('woo_shortname') ), max( $paged, $page ) );
	?></title>
	<meta name="description" content="<?php echo bloginfo('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<?php 
		$favicon = ( $woo_options['woohg_site_favicon'] == null ? $favicon = get_bloginfo( 'stylesheet_directory' )."/assets/images/favicon.ico" : $woo_options['woohg_site_favicon'] ); 
		$banner_img = $woo_options['woohg_home_banner'];
	?>
  <link href="<?php echo $favicon; ?>" rel="shortcut icon" type="image/x-icon" />
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->	
	<?php wp_head(); ?>

	<?php if(is_front_page() ) { ?>
	<script>
		if (document.documentElement.clientWidth > 800) {
			(function($) {
				$.vegas( 'slideshow', {
					delay: 8000,
					backgrounds: [
						{ src: 'wp-content/themes/wink/assets/images/banner.jpg', fade: 4000 },
						{ src: 'wp-content/themes/wink/assets/images/2.jpg', fade: 4000 },
						{ src: 'wp-content/themes/wink/assets/images/3.jpg', fade: 4000 }
					]
				})( 'overlay' );
			})(jQuery);
		}
	</script><?php
	} ?>
</head>

<body <?php body_class(); ?>>	

<div id="menu">
	<?php wp_nav_menu( 
			array( 'theme_location' => 'main-navigation', 
				   'menu_class' => '', 
				   'container' => false,
				   'container_class' => ''
			) 
	  ); 
	?>
</div>

<div id="wrap">
	<header>
		<div id="top-head">
			<div class="container">
				<div class="contact-number">
					<span class="icon-phone icon"></span> <?php echo $woo_options['woohg_contact_number'] ?>
				</div>

				<?php wp_nav_menu( 
					array( 'theme_location' => 'top-navigation', 
						   'menu_class' => '', 
						   'container' => false,
						   'container_class' => ''
						) 
				  ); 
				?>
			</div>		
		</div>		

		<div id="bottom-head">
			<div class="container">
				<div id="logo">
					<a href="<?php echo home_url(); ?>" class="icon icon-wink"></a>
				</div>
			</div>
			<div id="mobile-menu-trigger"><a class="mobile-menu-button icon icon-menu" href="#menu"></a></div>
		</div>

		<?php 
		$count = 0;
		if(isset($_SESSION['lightbox_data'])){
			$count = count($_SESSION['lightbox_data']);
		}else{
			$count = 0;
		}
		?>

		<div id="my-lightbox" class="container">
			<p><a href="<?php echo $woo_options['woohg_my_light_box_link'] ?>"><span class="icon icon-basket"></span><span class="label" href="#">My Lightbox </span><span class="count">(<?php echo $count;?>)</span></a></p>
		</div>
	</header>
	
	<div class="contact-number-view">
		<p><span class="icon-phone icon"></span> <?php echo $woo_options['woohg_contact_number'] ?></p>
	</div>

	<div id="content">