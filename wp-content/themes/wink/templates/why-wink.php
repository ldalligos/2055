<?php
/*
Template Name: Why Wink Template
*/

get_header(); 

global $woo_options; ?>
	<div class="container why-wink">
		<section class="primary">
			<div class="entry" role="main">
				<?php if( have_posts() ): ?>
					<h1 class="page-title"><?php the_title() ?></h1><?php 
					while( have_posts() ): the_post();
				        the_content();
	    			endwhile; 
	    		?>
	    		<section class="our-services">
					<div class="section-header"><h3 class="section-header-title">Our Services</h3></div>
					<?php echo get_post_meta(get_the_ID(), "ourServices", true); ?>
					<p class="download-document">
						<a class="btn-light" href="<?php echo $woo_options['woohg_service_document']; ?>" download="Our Services.pdf"><span class="icon icon-angle-down"></span>Download Credentials Document</a>
					</p>
	    			<div class="clearfix"></div>
	    		</section> <!-- .our-clients -->
	    		<section class="our-clients">
	    			<?php 
						$args = array(
				          'post_type' => 'client',
				          'posts_per_page' => -1, 
				          'post_status' => 'publish',
				        );
				        $clients = new WP_Query($args);
				        if ($clients->have_posts()) : 
				    ?>
						<div class="section-header"><h3 class="section-header-title">Our Clients</h3></div>
			    	<?php 	while( $clients->have_posts() ): $clients->the_post(); ?>
					    		<div class="client-logo-wrapper">
					    			<?php echo get_the_post_thumbnail(get_the_ID(), "post-thumbnail", array("title" => get_the_title())); ?>
					    		</div>
				    <?php
				     		endwhile;
				        endif;
	    			?>
	    			<div class="clearfix"></div>
	    		</section> <!-- .our-clients -->
	    		<section class="testimonials">
	    			<?php 
						$args = array(
				          'post_type' => 'testimonial',
				          'posts_per_page' => -1, 
				          'post_status' => 'publish',
				        );
				        $testimonials = new WP_Query($args);
				        if ($testimonials->have_posts()) : 
				    ?>
						<div class="section-header"><h3 class="section-header-title">Testimonials</h3></div>
			    	<?php while( $testimonials->have_posts() ): $testimonials->the_post(); ?>
					    		<div class="testimonial-wrapper">
					    			<span class="icon icon-quote"></span>
					    			<div class="block">
					    				<?php the_content(); ?>
					    			</div> 
					    		</div>
				    <?php
				     		endwhile;
				        endif;
	    			?>
	    			<div class="clearfix"></div>
	    		</section> <!-- .testimonials -->
	    		<section class="recent-work">
	    			<?php 
						$args = array(
				          'post_type' => 'case_study',
				          'posts_per_page' => 5, 
				          'post_status' => 'publish',
				          'orderby' => 'rand'
				        );
				        $works = new WP_Query($args);
				        if ($works->have_posts()) : 
				    ?>
						<div class="section-header"><h3 class="section-header-title">Recent Works</h3></div>
						<ul class="grid-layout">
			    	<?php while( $works->have_posts() ): $works->the_post(); ?>
					    		<li class="item">
					    			<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail(get_the_ID(), "post-thumbnail"); ?></a>
					    			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					    		</li>
				    <?php
				     		endwhile;
				        endif;
	    			?>
	    			</ul>
	    			<div class="clearfix"></div>
	    		</section> <!-- .recent-work -->
				<?php else: ?>
					<?php get_template_part( 'templates/partials/inc', 'noresult' ); ?>
				<?php endif; ?>
			</div><!-- END .entry -->
		</section><!-- END .primary -->
	</div>
<?php get_footer(); ?>