<?php
/*
Template Name: News Template
*/

get_header(); 

global $woo_options; ?>
	<div class="container contact-us news">
		<section class="primary">
			<div class="entry" role="main">
				<?php if( have_posts() ): ?>
					<h1 class="page-title"><?php the_title() ?></h1>
					<?php get_template_part( "templates/partials/social", "media" ); ?>
					<div class="styled-line clearfix"></div>
					<div class="posts"><?php

					require_once('twitteroauth.php');
					
					$twitteruser = $woo_options['woohg_twitter_username'] == "" ? "winkmpls" : $woo_options['woohg_twitter_username'];
					$notweets = 4; //how many tweets you want to retrieve
					$consumerkey = $woo_options['woohg_twitter_consumer_key'] == "" ? "IyJ7namXUfS4CJKE3Y7RQ" : $woo_options['woohg_twitter_consumer_key'];
					$consumersecret = $woo_options['woohg_twitter_consumer_secret_key'] == "" ? "6CRS3dlYCRFLwoIAap3AXm5YUfkOeHHUb8o0h4Ly8" : $woo_options['woohg_twitter_consumer_secret_key'];
					$accesstoken = $woo_options['woohg_twitter_access_token'] == "" ? "237212595-oHvZaTsZGKzZAGV2ikYj1HqhD1ODXYF5UsevmN6r" : $woo_options['woohg_twitter_access_token'];
					$accesstokensecret = $woo_options['woohg_twitter_access_secret_token'] == "" ? "1ICxNe10hvfhzgnLrjOWRDlnYvS7KRQCt3Oh2MF88uBFs" : $woo_options['woohg_twitter_access_secret_token'];

					function getConnectionWithAccessToken($cons_key, $cons_secret, $oauth_token, $oauth_token_secret) {
						$connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
						return $connection;
					}

					$connection = getConnectionWithAccessToken($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);

					$tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$twitteruser."&count=".$notweets);

					function fetchData($url){
						$curl_handle = curl_init();
						curl_setopt($curl_handle,CURLOPT_URL,$url);
						curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,20);
						curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
						$curl_data = curl_exec($curl_handle);
						curl_close($curl_handle);
						return $curl_data;
					}
					$instagram_user_id = $woo_options['woohg_instagram_user_id'];
					$instagram_access_token = $woo_options['woohg_instagram_access_token'] == "" ? "232967857.f59def8.c44c9e739efb4c52ab858bc84f420d00" : $woo_options['woohg_instagram_access_token'];
				    $instagrams = fetchData("https://api.instagram.com/v1/users/".$instagram_user_id."/media/recent/?access_token=".$instagram_access_token);
				    $instagrams = json_decode($instagrams, true);

					$i = 0;
					$instagramBlock = array();
					$twitterBlock = array();
					while($i < 6) {
						$rand = rand(1, 15);
						if (! in_array($rand, $instagramBlock)) {
							$instagramBlock[] = $rand;
							$i++;
						}
					}
					$i = 0;
					while($i < 4) {
						$rand = rand(1, 15);
						if (! in_array($rand, $instagramBlock) && ! in_array($rand, $twitterBlock)) {
							$twitterBlock[] = $rand;
							$i++;
						}
					}
					// $twitterBlock = array("2","4","9","11");
					// $instagramBlock = array("1","3","5","7","13","15");
					$twitterIndex = $instagramIndex = 0;
					$blockCtr = 1;
						while ($blockCtr <= 15) : ?>
							<?php if (in_array($blockCtr, $twitterBlock)): ?>
								<?php 
									if (count($tweets) == 4) :
										$tweet = $tweets[$twitterIndex];
										$blockContent = "<div class='block twitter-block'><span class='icon icon-twitter'></span>" . $tweet->text."<p class='user-name'>".$tweet->user->name."</p><p class='user-screen-name' >@".$tweet->user->screen_name."</p></div>";
									endif;
								?>
								<?php $twitterIndex++; ?>
							<?php elseif (in_array($blockCtr, $instagramBlock)): ?>
								<?php $instagram = $instagrams["data"][$instagramIndex]; ?>
									<?php $blockContent = "<div class='block instagram-block'><span class='icon icon-instagram'></span>" . '<a target="_blank" href="'.$instagram["link"].'" ><img src="'.$instagram["images"]["standard_resolution"]["url"].'" alt="" /></a></div>'; ?>
								<?php $instagramIndex++; ?>
							<?php 
								else:
									$blockContent = "<div class='block'></div>";
								endif; 
								echo $blockContent;
							?>
						<?php 
							$blockCtr++;
						endwhile;
					?>
					<div class="clearfix"></div>
				<?php else: ?>
					<?php get_template_part( 'templates/partials/inc', 'noresult' ); ?>
				<?php endif; ?>
			</div><!-- END .entry -->
		</section><!-- END .primary -->
	</div>
<?php get_footer(); ?>