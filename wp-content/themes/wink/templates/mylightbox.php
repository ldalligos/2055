<?php
/*
Template Name: My Lightbox Page
*/


//If the form is submitted
if( isset( $_POST['submitted'] ) ) {
	$response = array("status" => false);
	$url = $_POST['url'];
	if(isset($_SESSION['lightbox_data'])){
		$ids = $_SESSION['lightbox_data'];
		if (strpos($url, "?") !== false) {
			$url .="&mid=";
		} else {
			$url .= "?mid=";
		}
		$len = count($ids); $ctr = 0;
		foreach ($ids as $id){
			$url.=$id;
			$ctr++;
			if ($ctr < $len) {
				$url.=",";
			}
		}
	}

	//Check to make sure that the recipient_name field is not empty
	if( trim( $_POST['recipient_name'] ) === '' ) {
		$response["recipientNameError"] =  __( 'You forgot to enter the recipient name.', 'woothemes' );
		$hasError = true;
	} else {
		$recipient_name = trim( $_POST['recipient_name'] );
	}
	
	//Check to make sure that the name field is not empty
	if( trim( $_POST['sender_name'] ) === '' ) {
		$response["senderNameError"] =  __( 'You forgot to enter the sender name.', 'woothemes' );
		$hasError = true;
	} else {
		$sender_name = trim( $_POST['sender_name'] );
	}

	//Check to make sure sure that a valid recipient email address is submitted
	if( trim( $_POST['recipient_email'] ) === '' )  {
		$response["recipientEmailError"] = __( 'You forgot to enter the recipient email address.', 'woothemes' );
		$hasError = true;
	} else if ( ! eregi( "^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim( $_POST['recipient_email'] ) ) ) {
			$response["recipientEmailError"] = __( 'You entered an invalid recipient email address.', 'woothemes' );
			$hasError = true;
		} else {
		$recipient_email = trim( $_POST['recipient_email'] );
	}
	//Check to make sure sure that a valid sender email address is submitted
	if( trim( $_POST['sender_email'] ) === '' )  {
		$response["senderEmailError"] = __( 'You forgot to enter the sender email address.', 'woothemes' );
		$hasError = true;
	} else if ( ! eregi( "^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim( $_POST['sender_email'] ) ) ) {
			$response["senderEmailError"] = __( 'You entered an invalid sender email address.', 'woothemes' );
			$hasError = true;
		} else {
		$sender_email = trim( $_POST['sender_email'] );
	}

	$message = stripslashes( trim( $_POST['message'] ) );

	//If there is no error, send the email
	if( ! isset( $hasError ) ) {
		$emailTo = $recipient_email;
		$subject = $sender_name . __( ' shared a lightbox from Wink models.', 'woothemes' );
		$body = $message."\n\r\n\r wink model lightbox link: ".$url;
		$headers = __( 'From: ', 'woothemes' ) . "$sender_name <$sender_email>";

		wp_mail( $emailTo, $subject, $body, $headers );

		$response["status"] = true;

	}
	echo json_encode( $response );
	exit();
}

get_header(); 

global $woo_options; ?>
	<div class="container my-light-box">
	<section class="primary">
		<div class="entry" role="main">
			<div class="head">
				<h1 class="post-title">My Lightbox</h1>
				<div class="item-buttons top">
					<a class="share-btn btn" href="#"><span class="icon icon-mail"></span>Share</a>
						<a class="comp-btn btn" href="#"><span class="icon icon-user"></span>Download all Comps</a>
				</div>
			</div>

			<section class="share">
				<form class="" id="share" action="<?php the_permalink(); ?>" method="post">
					<h3>Share this model with a friend</h3>
					<div class="col-1">
						<input type="text" id="txt_senderName" placeholder="Your Name *" class="requiredField" >
						<input type="text" id="txt_senderEmail" placeholder="Your Email *" class="requiredField email" >
						<input type="text" id="txt_recipientName" placeholder="Recipient Name *" class="requiredField" >
						<input type="text" id="txt_recipientEmail" placeholder="Recipient Email *" class="requiredField email" >
					</div>
					<div class="col-2">
						<textarea id="txt_message" placeholder="Your Message"></textarea>
					</div>
					<div class="message"></div> <input type="submit" value="SUBMIT" name="btn_submit" id="btn_submit" />
					<div class="clearfix"></div>
				</form>
			</section>
			<?php if( have_posts() ): while( have_posts() ): the_post();				

				$ids = '';

				if(isset($_GET['mid'])){
					//Query
					$ids = explode(",",$_GET['mid']);
				
				}else{

					if(isset($_SESSION['lightbox_data'])){
						$ids = $_SESSION['lightbox_data'];
					}
				}
				

				$args = array(
		          'post_type' => 'model',
		          'post_status' => 'publish',
		          'post__in' => $ids
		          //'paged' => $paged
		        );

	        $model_query = new WP_Query( $args );

	        require( locate_template( 'templates/partials/loop-mylightbox.php' ) );
        
        // Display page content
        the_content();
      ?>
      <?php endwhile; ?>
				
			<?php else: ?>
				<?php get_template_part( 'templates/partials/inc', 'noresult' ); ?>
			<?php endif; ?>
		</div><!-- END .entry -->
	</section><!-- END .primary -->

	<?php //get_sidebar(); ?>
	</div>


<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function($){
	$(document).ready(function() {
		$("form#share").submit(function(e) {
			e.preventDefault();
			var hasError = false;
			$( '.requiredField', this).each(function() {
				if($.trim($(this).val()) == '') {
					$(this).addClass( 'inputError' );
					hasError = true;
				} else if($(this).hasClass( 'email')) {
					var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					if(!emailReg.test($.trim($(this).val()))) {
						$(this).addClass( 'inputError' );
						hasError = true;
					}
				}
			});
			if(!hasError) {
				$(".share .message").text("Loading... Please wait.").addClass("loading").removeClass("error");
				$.ajax({
				    url : $(this).attr("action"),
				    type: "POST",
				    data : {
				    	sender_name: $("#txt_senderName").val(),
				    	sender_email: $("#txt_senderEmail").val(),
				    	recipient_name: $("#txt_recipientName").val(),
				    	recipient_email: $("#txt_recipientEmail").val(),
				    	message: $("#txt_message").val(),
				    	url: location.href,
				    	submitted: true
				    },
				    dataType : "json",
				    success: function(data, textStatus, jqXHR) {
				    	if(data["status"]) {
				    		$(".share .message").text("You have successfully shared the model.").addClass("success").removeClass("error loading");
				    	}
				    },
				    error: function (jqXHR, textStatus, errorThrown) {
				    	$(".share .message").text("Can not send your email. Something went wrong. Sorry.").addClass("error").removeClass("loading success");
				 		console.log("Can not send your email. Something went wrong. Sorry.")
				    }
				});
			} else {
				$(".share .message").text("You must fill in the items highlighted in red to proceed.").addClass("error").removeClass("loading success");
			}
		});

	});
})(jQuery)
//-->!]]>
</script>
<?php get_footer(); ?>