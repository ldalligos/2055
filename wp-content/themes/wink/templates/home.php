<?php
/*
Template Name: Home Page
*/

get_header(); 

global $woo_options; ?>

	<div class="home-accordion">
		<div id="open-content-wrap"><p class="icon icon-plus"></p></div>
		<div id="home-content-wrap" class="group">
			<ul class="model-category-list">
			<!-- START query for Model categories -->
			<?php 
				$categories = get_categories('title_li=&orderby=ID&order=ASC&hide_empty=0&taxonomy=model-category');
					
				$x = 0;
				$terms = apply_filters( 'taxonomy-images-get-terms', '', array('taxonomy' => 'model-category') );

				$cat_pages = array($woo_options['woohg_men_page'], $woo_options['woohg_women_page'], $woo_options['woohg_classics_page'], $woo_options['woohg_sports_page'], $woo_options['woohg_promotions_page']);
				$cat_page_index = 0;
				//print_r($terms);
				foreach( $categories as $category ) {
					//print_r($category);
					if($category->category_parent == 0){
					echo '<li class="model-category-container">';
					//print_r($category);					
				  	
				  	$temp_total_term = count( $terms );
				  	for( $c = 0; $c < $temp_total_term; $c++ ) {

					  	if( $terms[$c]->term_id == $category->term_id ) {
					  		
					  		$cat_image = wp_get_attachment_url( $terms[$c]->image_id, 'full' );
					  		echo '<a href="' . get_permalink($cat_pages[$cat_page_index]) . '"><img src="' . $cat_image . '" alt="' . $category->name . '" /></a>';
					    }
					}    
				  		
			  	echo '<div class="title-container">';	
					echo '<h3><a href="' . get_term_link( $category->slug, $category->taxonomy ) . '">'. $category->name . '</a></h3>';
					echo '<p>' . $category->description . '</p>';
					echo '</div>';
					$x++;
					echo '</li>';
				}
				$cat_page_index++;
				} // END foreach
			?>
			<!-- END query -->
			</ul>
			<div id="content-bottom">
				<div class="welcome-page-wrap">
					<?php 
						$post = get_page($woo_options['woohg_custom_page']); 
						$content = apply_filters('the_excerpt', $post->post_excerpt); 
						echo $content;
					?>
					<a href="<?php echo get_permalink($woo_options['woohg_custom_page']); ?>">Read more</a>
				</div>

				<div class="case-studies-wrap">
					<?php if( is_dynamic_sidebar( 'case-studies-widget' ) ): ?>
						<?php dynamic_sidebar( 'case-studies-widget' ); ?>
					<?php endif; ?>
				</div>
				
				<div class="testimonials-wrap">
					<?php if( is_dynamic_sidebar( 'testimonials-widget' ) ): ?>
						<?php dynamic_sidebar( 'testimonials-widget' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div><!-- END #home-content-wrap -->
	</div>

<?php get_footer(); ?>