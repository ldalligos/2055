<?php global $woo_options; ?>

<div class="social-media-icons">
	<p class="heading">Follow Us:</p>
	<ul>
		<li class="facebook">
			<a href="<?php echo $woo_options['woohg_facebook_url'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri() . '/assets/images/fb@2x.png'; ?>" ></a>
		</li>
		<li class="twitter">
			<a href="<?php echo $woo_options['woohg_twitter_url'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri() . '/assets/images/twitter@2x.png'; ?>" ></a></li>
		<li class="instagram">
			<a href="<?php echo $woo_options['woohg_instagram_url'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri() . '/assets/images/instagram@2x.png'; ?>" ></a>
		</li>
		<li class="linkedin">
			<a href="<?php echo $woo_options['woohg_linkedin_url'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri() . '/assets/images/linkedin@2x.png'; ?>" ></a>
		</li>
	</ul>
</div>