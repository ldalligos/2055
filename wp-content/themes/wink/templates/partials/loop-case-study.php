<?php global $woo_options; if ($case_studies_query->have_posts()) : ?>
  
  <div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
      bcn_display();
    }?>
  </div>
  
  <ul class="grid-layout">
  
    <!-- The Loop -->
    <?php 
      while ($case_studies_query->have_posts()) : $case_studies_query->the_post(); 
      
      //include get_template_directory() . '/lib/variables.php';

      $featured_image  = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium');
    ?>

      <li class="item">

        <a href="<?php the_permalink(); ?>" class="pull-left"><img src="<?php echo $featured_image[0]; ?>" alt="" class="media-object"></a>
        
        <div class="head">
          <h2 class="item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        </div>      
  
      </li><!-- END .list-item -->

    <?php endwhile; ?>
  </ul>
  <!-- END of the loop -->


  <?php 
    //wp_pagenavi( array( 'query' => $model_query ) ); 
    wp_reset_postdata();
  ?>
 
<?php else:  ?>
  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; 