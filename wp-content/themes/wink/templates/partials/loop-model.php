<?php global $woo_options; if ($model_query->have_posts()) : ?>
  <div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
      bcn_display();
    }?>
  </div>
  <ul class="grid-layout">
  
  <!-- The Loop -->
  <?php 
    while ($model_query->have_posts()) : $model_query->the_post(); 
    
    //include get_template_directory() . '/lib/variables.php';

    $featured_image  = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
    $featured_image_url = $featured_image[0];

    $resized_image = aq_resize( wp_get_attachment_url( get_post_thumbnail_id() ,'full' ), 187, 187, true ); //resize & crop the image

    $availability = get_post_meta( $id, 'model_availability', true);
  ?>
    <li class="item">

      <a href="<?php the_permalink(); ?>" class="pull-left"><img src="<?php echo $featured_image_url; ?>" alt="" class="media-object"></a>
      
      <div class="head">
        <h2 class="item-title">
          <a href="<?php the_permalink(); ?>">
            <?php 
              $title = get_the_title(); 
              $title = str_replace(' ', '<br />', $title);
              echo $title
            ?>
          </a>
        </h2>
        <?php if ($availability == 'No') { ?>
          <span class="icon icon-airplane"></span>
        <?php } ?>
      </div>      

      <div class="item-buttons">

        <a class="add-btn btn add-model" href="#" data-post_id="<?php echo $post->ID;?>"><span class="icon icon-heart"></span>Add</a>
        <a class="comp-btn btn" href="#"><span class="icon icon-user"></span>Comp</a>
      </div>
          
    </li><!-- END .list-item -->
  <?php endwhile; ?>
  </ul>
  <!-- END of the loop -->

  <!-- Ajax hiddend fields -->
  <?php
   $nonce = wp_create_nonce("my_user_vote_nonce");
   $link = admin_url('admin-ajax.php?action=add_model_to_lightbox&post_id='.$post->ID.'&nonce='.$nonce);
  
    //echo '<a class="add-btn btn add-model" data-nonce="' . $nonce . '" data-post_id="' . $post->ID . '" href="' . $link . '"><span class="icon icon-heart"></span>Add</a>';
  ?>

  <input type="hidden" name="ajax-nonce" value="<?php echo $nonce;?>" id="ajax-nonce" />

  <?php 
    //wp_pagenavi( array( 'query' => $model_query ) ); 
    wp_reset_postdata();
  ?>
 
<?php else:  ?>
  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; 