<?php global $woo_options; if ($model_query->have_posts()) : ?>

  <ul class="grid-layout">
  
  <!-- The Loop -->
  <?php 
    while ($model_query->have_posts()) : $model_query->the_post(); 
    
    //include get_template_directory() . '/lib/variables.php';

    $featured_image  = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium');
    $availability = get_post_meta( $id, 'model_availability', true);
  ?>
    <li class="item" id="<?php echo $post->ID;?>">

      <a href="<?php the_permalink(); ?>" class="pull-left"><img src="<?php echo $featured_image[0]; ?>" alt="" class="media-object"></a>
      
      <div class="head">
        <h2 class="item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      </div>      

      <div class="item-buttons inside">

        <a class="add-btn btn remove-model" href="#" data-post_id="<?php echo $post->ID;?>"><span class="icon icon-cancel-circle"></span>Remove</a>
        <a class="comp-btn btn" href="#"><span class="icon icon-user"></span>Comp</a>
      </div>
          
    </li><!-- END .list-item -->
  <?php endwhile; ?>
  </ul>
  <!-- END of the loop -->

  <!-- Ajax hiddend fields -->
  <?php
   $nonce = wp_create_nonce("my_user_vote_nonce");
   $link = admin_url('admin-ajax.php?action=add_model_to_lightbox&post_id='.$post->ID.'&nonce='.$nonce);
  
    //echo '<a class="add-btn btn add-model" data-nonce="' . $nonce . '" data-post_id="' . $post->ID . '" href="' . $link . '"><span class="icon icon-heart"></span>Add</a>';
  ?>

  <input type="hidden" name="ajax-nonce" value="<?php echo $nonce;?>" id="ajax-nonce" />

  <?php 
    //wp_pagenavi( array( 'query' => $model_query ) ); 
    wp_reset_postdata();
  ?>
 
<?php else:  ?>
  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; 