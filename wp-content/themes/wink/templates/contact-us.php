<?php
/*
Template Name: Contact Us Template
*/

get_header(); 

global $woo_options; ?>
	<div class="container contact-us">
		<section class="primary">
			<div class="entry" role="main">

				<?php if( have_posts() ): ?>
					<h1 class="page-title"><?php the_title() ?></h1><?php
					while( have_posts() ): the_post();
				        the_content();
	    			endwhile; 
	    		?>
	    		<div class="col-1">
		    		<section class="contact-form" >
		    			<?php echo do_shortcode(get_post_meta( get_the_ID(), "contactForm7", true )); ?>
		    		</section>
		    		<section class="join-wink">
		    			<div class="image-wrapper">
		    				<a href="<?php echo get_permalink($woo_options['woohg_join_wink_page']); ?>">
		    					<img src="<?php echo $woo_options['woohg_join_wink_image']; ?>" alt="" />
			    			</a>
		    			</div>
		    		</section>
		    	</div>
	    		<div class="col-2">
		    		<section class="our-location">
						<h3>our location</h3>
		    			<div class="map-details">
		    				<?php echo get_post_meta( get_the_ID(), "googleMap", true ) ?>
		    				<h4>WINK MODELS</h4>
		    				<span class="phone-number"><span class="icon icon-phone"></span>+61 02 8005 4388</span></p>
		    				<address>
		    					Suite 403/19A Boundary St<br />
		    					Rushcutters Bay<br />
		    					NSW 2011<br />
		    					Australia
		    				</address>
		    			</div>
		    		</section>
		    		<section class="our-people">
		    			<?php 
							$args = array(
					          'post_type' => 'our-people',
					          'posts_per_page' => -1, 
					          'post_status' => 'publish',
					        );
					        $personnels = new WP_Query($args);
					        if ($personnels->have_posts()) : 
					    ?>
							<h3>our people</h3>
				    	<?php 	while( $personnels->have_posts() ): $personnels->the_post(); ?>
						    		<div class="personnel">
						    			<div class="image-wrapper">
						    				<?php echo get_the_post_thumbnail(get_the_ID(), "post-thumbnail"); ?>
						    			</div>
						    			<div class="personnel-info">
						    				<span class="name"><?php the_title(); ?></span>
						    				<span class="position"><?php echo get_post_meta( get_the_ID(), 'position', true ) ?></span>
						    				<span class="email"><a href="mailto:<?php echo get_post_meta( get_the_ID(), 'email', true ) ?>"><?php echo get_post_meta( get_the_ID(), 'email', true ) ?></a></span>
						    				<span class="contact-number"><?php echo get_post_meta( get_the_ID(), 'contact_number', true ) ?></span>
						    			</div>
						    		</div>
					    <?php
					     		endwhile;
					        endif;
		    			?>
		    		</section>
		    	</div>
				<?php else: ?>
					<?php get_template_part( 'templates/partials/inc', 'noresult' ); ?>
				<?php endif; ?>
			</div><!-- END .entry -->
		</section><!-- END .primary -->
	</div>
<?php get_footer(); ?>