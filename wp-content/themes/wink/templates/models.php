<?php
/*
Template Name: Models Template
*/

get_header(); 

global $woo_options; ?>
	<div class="container models">
	<section class="primary">
		<div class="entry" role="main">
			<?php if( have_posts() ): while( have_posts() ): the_post();				

        $number_of_models_to_display = $woo_options['woohg_no_models'];
				$model_category = get_post_meta( $post->ID, 'model_category', true );
				//echo $model_category;
				if($model_category === 'select-model'){

				$args = array(
          'post_type' => 'model',
          'posts_per_page' => $number_of_models_to_display, 
          'post_status' => 'publish',
          'order' => 'DESC', 
          'orderby' => 'date'
          //'paged' => $paged
        );

				}else{

					$args = array(
				          'post_type' => 'model',
				          'posts_per_page' => $number_of_models_to_display, 
				          'post_status' => 'publish',
				          'order' => 'DESC', 
				          'orderby' => 'date',
				          //'paged' => $paged
				          'tax_query' => array(
								array(
									'taxonomy' => 'model-category',
									'field' => 'slug',
									'terms' => $model_category
								)
							)
				       );

				}
					//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	       		

					

        $model_query = new WP_Query( $args );

        require( locate_template( 'templates/partials/loop-model.php' ) );
        
        // Display page content
        the_content();
      ?>
      <?php endwhile; ?>
				
			<?php else: ?>
				<?php get_template_part( 'templates/partials/inc', 'noresult' ); ?>
			<?php endif; ?>
		</div><!-- END .entry -->
	</section><!-- END .primary -->

	<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>