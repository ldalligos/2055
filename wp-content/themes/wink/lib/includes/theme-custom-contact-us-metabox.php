<?php
/*
 *Program Name : Custom Page Metaboxes
*/

// Metaboxes for contact us page template
add_action( 'add_meta_boxes', 'add_contact_us_meta_box' );
add_action( 'save_post', 'dynamic_save_contact_us_meta_box' );

function add_contact_us_meta_box() {
    global $post;
    if (get_post_meta( $post->ID, '_wp_page_template', true ) != "") {
        if ( preg_match('/$contact-us.php/i', get_post_meta( $post->ID, '_wp_page_template', true )) ) {
            add_meta_box('dynamic_sectionid', __( 'Wink Theme Custom Settings', 'myplugin_textdomain' ), 'contact_us_meta_box', 'page');
        }
    }
}

function contact_us_meta_box() {
    global $post;
    wp_nonce_field( plugin_basename( __FILE__ ), 'dynamicMeta_noncename' );
    $contactForm7 = get_post_meta( $post->ID, 'contactForm7', true );
    $googleMap = get_post_meta( $post->ID, 'googleMap', true );
    ?>
    
    <table class="woo_metaboxes_table">
        <tbody>
            <tr class="woo-custom-field woo-field-type-text">        
                <th class="woo_metabox_names"><label class="custom-meta-label" for="contactForm7">Contact Form 7 Shortcode: </label></th>
                <td><input class="woo_input_text" type="text" name="contactForm7" id="contactForm7" value='<?php echo $contactForm7; ?>'><span class="woo_metabox_desc">Enter the contact form 7 shortcode here.</span></td>
            </tr>
            <tr class="woo-custom-field woo-field-type-text">        
                <th class="woo_metabox_names"><label class="custom-meta-label" for="googleMap">Google Map Embed Code: </label></th>
                <td>
                    <textarea rows="5" class="woo_input_text" type="text" name="googleMap" id="googleMap"><?php echo $googleMap; ?></textarea>
                    <span class="woo_metabox_desc">Enter the google map embed code here.</span>
                </td>
            </tr>
        </tbody>
    </table>
    <?php 
}

function dynamic_save_contact_us_meta_box( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
        return;
    if ( !isset( $_POST['dynamicMeta_noncename'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename'], plugin_basename( __FILE__ ) ) )
        return;

    update_post_meta( $post_id, 'contactForm7', $_POST['contactForm7'] );
    update_post_meta( $post_id, 'googleMap', $_POST['googleMap'] );
}

?>