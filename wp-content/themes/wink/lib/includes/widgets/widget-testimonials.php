<?php
/*
 * Program Name : Testimonials Widget
 */

class thg_testimonials_widget extends WP_Widget{
	
	function thg_testimonials_widget(){
		$widget_options = array('description' => __('A widget to display testmonials'));
		parent::WP_Widget(false,__('Testimonials'),$widget_options);
	}
	
	function widget($args, $instance){
		extract($args, EXTR_SKIP);
		
		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Testimonials' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$r = new WP_Query( array( 'post_type' => 'testimonial', 
															'no_found_rows' => true, 
															'post_status' => 'publish', 
															'ignore_sticky_posts' => true ) );
		if ($r->have_posts()) : ?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<ul id="home-testimonial-list" class="testimonial-list">
		
		<?php while ( $r->have_posts() ) : $r->the_post(); ?>
			<li class="">
				<span class="icon icon-quote"></span>	
        <?php the_content(); ?>
			</li>
		<?php endwhile; ?>
		</ul>

		<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;
	}
	
	function update($new_instance,$old_instance){
		$instance = $old_instance;
		$instance['title'] = stripslashes($new_instance['title']);
		
		return $instance;
	}
	
	function form($instance){
		$title = htmlspecialchars($instance['title']);
		echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title:' . '</label><input class="" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" /></p>';
	}
}

function thg_testimonials_widget_init(){
	register_widget('thg_testimonials_widget');
}
	
add_action('widgets_init','thg_testimonials_widget_init');	
?>