<?php
/*
 * Program Name : Case Studies Widget
 */

class thg_case_studies_widget extends WP_Widget{
	
	function thg_case_studies_widget(){
		$widget_options = array('description' => __('A widget to display case studies'));
		parent::WP_Widget(false,__('Case Studies'),$widget_options);
	}
	
	function widget($args, $instance){
		extract($args, EXTR_SKIP);
		
		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Testimonials' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$s = new WP_Query( array( 'post_type' => 'case_study', 
															'no_found_rows' => true, 
															'post_status' => 'publish',
															'posts_per_page' => '1',
															'ignore_sticky_posts' => true ) );
		if ($s->have_posts()) : ?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<ul>
		<?php while ( $s->have_posts() ) : $s->the_post(); ?>
			<li class="">
        <?php
	        $thumb = get_post_thumbnail_id( $post_id );
	        $img_url = wp_get_attachment_url( $thumb );
	        $image = aq_resize( $img_url, 192, 113, true ); ?>
	        <img src="<?php echo $img_url ?>" width="262" alt=""><?php
        ?>
			</li>
		<?php endwhile; ?>
		</ul>

		<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;		
	}
	
	function update($new_instance,$old_instance){
		$instance = $old_instance;
		$instance['title'] = stripslashes($new_instance['title']);
		
		return $instance;
	}
	
	function form($instance){
		$title = htmlspecialchars($instance['title']);
		echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title:' . '</label><input class="" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" /></p>';
	}
}

function thg_case_studies_widget(){
	register_widget('thg_case_studies_widget');
}
	
add_action('widgets_init','thg_case_studies_widget');	
?>