<?php

function woo_options() {
	// VARIABLES
	$themename = "Wink Theme";
	$shortname = "woohg";
	$manualurl = "";

	$GLOBALS['template_path'] = get_bloginfo('template_directory');
	$theme_url = $GLOBALS['template_path'];
	
	//Access the WordPress Categories via an Array
	$woo_categories = array();  
	$woo_categories_obj = get_categories('hide_empty=0');
	foreach ($woo_categories_obj as $woo_cat) { $woo_categories[$woo_cat->cat_ID] = $woo_cat->cat_name; }
	$categories_tmp = array_unshift($woo_categories, "Select a category:");    
		   
	//Access the WordPress Pages via an Array
	$woo_pages = array("Select a page:");
	$woo_pages_obj = get_pages('sort_column=post_parent,menu_order');    
	foreach ($woo_pages_obj as $woo_page) { $woo_pages[$woo_page->ID] = $woo_page->post_title; }

	//More Options
	$images_dir =  get_template_directory_uri() . '/lib/functions/images/';
	$includes_images_dir =  get_template_directory_uri() . '/lib/includes/images/';

	// THIS IS THE DIFFERENT FIELDS
	$options = array();   

	$options[] = array( "name" => "Branding and Identity",
						"icon" => "misc",
						"type" => "heading");

	$options[] = array( "name" => "Site Logo",
						"desc" => "Upload your site logo here.",
						"id" => $shortname."_site_logo",
						"std" => "",
						"type" => "upload");						
	$options[] = array( "name" => "Site Favicon",
						"desc" => "Upload your site favicon here.",
						"id" => $shortname."_site_favicon",
						"std" => "",
						"type" => "upload");	
	$options[] = array( "name" => "Contact Number",
						"desc" => "Enter the contact number",
						"id" => $shortname."_contact_number",
						"std" => "",
						"type" => "text");

	$options[] = array( "name" => "Home Page Settings",
						"icon" => "misc",
						"type" => "heading");
	$options[] = array( "name" => "Home Banner",
						"desc" => "Upload the home page banner here.",
						"id" => $shortname."_home_banner",
						"std" => "",
						"type" => "upload");
	$options[] = array( "name" => "Custom Page on Home",
						"desc" => "Select the page to show on home page.",
						"id" => $shortname."_custom_page",
						"std" => "",
						"type" => "select2",
						'options' => $woo_pages );

	$options[] = array( "name" => "Men Page",
						"desc" => "Select the page for Men.",
						"id" => $shortname."_men_page",
						"std" => "",
						"type" => "select2",
						'options' => $woo_pages );
	$options[] = array( "name" => "Women Page",
						"desc" => "Select the page for Women.",
						"id" => $shortname."_women_page",
						"std" => "",
						"type" => "select2",
						'options' => $woo_pages );
	$options[] = array( "name" => "Classics Page",
						"desc" => "Select the page for Classics.",
						"id" => $shortname."_classics_page",
						"std" => "",
						"type" => "select2",
						'options' => $woo_pages );
	$options[] = array( "name" => "Sports Page",
						"desc" => "Select the page for Men.",
						"id" => $shortname."_sports_page",
						"std" => "",
						"type" => "select2",
						'options' => $woo_pages );
	$options[] = array( "name" => "Promotions Page",
						"desc" => "Select the page for Promotions.",
						"id" => $shortname."_promotions_page",
						"std" => "",
						"type" => "select2",
						'options' => $woo_pages );
	

	$options[] = array( "name" => "Models Settings",
						"icon" => "misc",
						"type" => "heading");
	$options[] = array( "name" => "Number of models to show",
						"desc" => "Enter the number of models that to show across pages",
						"id" => $shortname."_no_models",
						"std" => "",
						"type" => "text");



	$options[] = array( "name" => "Header",
						"icon" => "misc",
						"type" => "heading");

	$options[] = array( "name" => "My Lightbox Page",
						"desc" => "Enter the page URL for My Lightbox Page",
						"id" => $shortname."_my_light_box_link",
						"std" => "",
						"type" => "text");

	/* START NICK */
	$options[] = array( "name" => "Contact Us Page Settings",
						"icon" => "misc",
						"type" => "heading");

		$options[] = array( "name" => "Join Wink Page Link",
							"desc" => "Select the join wink page.",
							"id" => $shortname."_join_wink_page",
							"std" => "",
							"type" => "select2",
							'options' => $woo_pages );

		$options[] = array( "name" => "Join Wink Image",
							"desc" => "Upload the join wink image here.",
							"id" => $shortname."_join_wink_image",
							"std" => "",
							"type" => "upload" );

		$options[] = array( "name" => "Our Services PDF Document",
							"desc" => "Upload the our services pdf document here.",
							"id" => $shortname."_service_document",
							"std" => "",
							"type" => "upload" );

		$options[] = array( "name" => "News Page Settings",
						"icon" => "misc",
						"type" => "heading");

		$options[] = array( "name" => "Twitter Username",
							"desc" => "Enter the username you want to reference in twitter.",
							"id" => $shortname."_twitter_username",
							"std" => "",
							"type" => "text");

		$options[] = array( "name" => "Twitter Consumer Key",
							"desc" => "Enter the twitter consumer key. Get the twitter keys and token here, please create twitter application, https://dev.twitter.com/apps.",
							"id" => $shortname."_twitter_consumer_key",
							"std" => "",
							"type" => "text");

		$options[] = array( "name" => "Twitter Consumer Secret Key",
							"desc" => "Enter the twitter consumer secret key.",
							"id" => $shortname."_twitter_consumer_secret_key",
							"std" => "",
							"type" => "text");

		$options[] = array( "name" => "Twitter Access Token",
							"desc" => "Enter the twitter access token.",
							"id" => $shortname."_twitter_access_token",
							"std" => "",
							"type" => "text");

		$options[] = array( "name" => "Twitter Access Secret Token",
							"desc" => "Enter the twitter access secret token.",
							"id" => $shortname."_twitter_access_secret_token",
							"std" => "",
							"type" => "text");

		$options[] = array( "name" => "Instagram User ID",
							"desc" => "Enter the instagram user id. Get the instagram user id here http://jelled.com/instagram/lookup-user-id.",
							"id" => $shortname."_instagram_user_id",
							"std" => "",
							"type" => "text");

		$options[] = array( "name" => "Instagram Access Token",
							"desc" => "Enter the instagram access token. Get your access token in instagram.",
							"id" => $shortname."_instagram_access_token",
							"std" => "",
							"value" => "232967857.f59def8.c44c9e739efb4c52ab858bc84f420d00",
							"type" => "text");


		$options[] = array( "name" => "Social Profiles",
						"icon" => "misc",
						"type" => "heading");

		$options[] = array( "name" => "Facebook Profile",
							"desc" => "Enter your Facebook Page URL.",
							"id" => $shortname."_facebook_url",
							"std" => "",
							"type" => "text");
		$options[] = array( "name" => "Twitter Profile",
							"desc" => "Enter your Twitter Page URL.",
							"id" => $shortname."_twitter_url",
							"std" => "",
							"type" => "text");
		$options[] = array( "name" => "Instagram Profile",
							"desc" => "Enter your Instagram Page URL.",
							"id" => $shortname."_instagram_url",
							"std" => "",
							"type" => "text");
		$options[] = array( "name" => "LinkedIn Profile",
							"desc" => "Enter your LinkedIn Page URL.",
							"id" => $shortname."_linkedin_url",
							"std" => "",
							"type" => "text");

	/* END NICK */

	// Add extra options through function
	if ( function_exists("woo_options_add") )
		$options = woo_options_add($options);

	if ( get_option('woo_template') != $options) update_option('woo_template',$options);      
	if ( get_option('woo_themename') != $themename) update_option('woo_themename',$themename);   
	if ( get_option('woo_shortname') != $shortname) update_option('woo_shortname',$shortname);
				  
				  
	// Woo Metabox Options
	include( 'theme-metabox.php' );			
	

	// Add extra metaboxes through function
	if ( function_exists("woo_metaboxes_add") )
		$woo_metaboxes = woo_metaboxes_add($woo_metaboxes);
		
	if ( get_option('woo_custom_template') != $woo_metaboxes) update_option('woo_custom_template',$woo_metaboxes);   


	remove_meta_box( 'woothemes-settings' , 'page' , 'normal' );	


}

add_action( 'admin_head','woo_options' );  
add_action( 'init', 'woo_global_options' );
function woo_global_options(){
	global $woo_options;
	$woo_options = get_option( 'woo_options' ); 
}	

?>