<?php
/*
 *Program Name : Custom Post Type
 */

class thg_our_people_custom_post_type_class {
	
	// Register Custom Post Type
	function thg_register_our_people_post_type() {

		$support_editors = array(
			'title', 
			'thumbnail'
		);

		$labels = array(
			'name'                => _x( 'Our People', 'Post Type General Name', 'thg' ),
			'singular_name'       => _x( 'Our People', 'Post Type Singular Name', 'thg' ),
			'menu_name'           => __( 'Our People', 'thg' ),
			'parent_item_colon'   => __( 'Parent Our People:', 'thg' ),
			'all_items'           => __( 'All Our People', 'thg' ),
			'view_item'           => __( 'View Our People', 'thg' ),
			'add_new_item'        => __( 'Add New Our People', 'thg' ),
			'add_new'             => __( 'New Our People', 'thg' ),
			'edit_item'           => __( 'Edit Our People', 'thg' ),
			'update_item'         => __( 'Update Our People', 'thg' ),
			'search_items'        => __( 'Search our people', 'thg' ),
			'not_found'           => __( 'No our people found', 'thg' ),
			'not_found_in_trash'  => __( 'No our people found in Trash', 'thg' ),
		);

		$args = array(
			'label'               => __( 'Our People', 'thg' ),
			'description'         => __( 'Our People information pages', 'thg' ),
			'labels'              => $labels,
			'supports'            => $support_editors,
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		register_post_type( 'our-people', $args );
	}

} // END class

// Add our client post type
add_action('init',array('thg_our_people_custom_post_type_class','thg_register_our_people_post_type'));

?>