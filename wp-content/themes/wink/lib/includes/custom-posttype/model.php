<?php
/*
 *Program Name : Custom Post Type
 */

class thg_model_custom_post_type_class {
	
	// Register Custom Post Type
	function thg_register_model_post_type() {

		$support_editors = array(
			'title', 
			'editor', 
			'excerpt', 
			'author', 
			'thumbnail'
		);

		$labels = array(
			'name'                => _x( 'Models', 'Post Type General Name', 'thg' ),
			'singular_name'       => _x( 'Model', 'Post Type Singular Name', 'thg' ),
			'menu_name'           => __( 'Model', 'thg' ),
			'parent_item_colon'   => __( 'Parent Model:', 'thg' ),
			'all_items'           => __( 'All Models', 'thg' ),
			'view_item'           => __( 'View Model', 'thg' ),
			'add_new_item'        => __( 'Add New Model', 'thg' ),
			'add_new'             => __( 'New Model', 'thg' ),
			'edit_item'           => __( 'Edit Model', 'thg' ),
			'update_item'         => __( 'Update Model', 'thg' ),
			'search_items'        => __( 'Search models', 'thg' ),
			'not_found'           => __( 'No models found', 'thg' ),
			'not_found_in_trash'  => __( 'No models found in Trash', 'thg' ),
		);

		$args = array(
			'label'               => __( 'model', 'thg' ),
			'description'         => __( 'Model information pages', 'thg' ),
			'labels'              => $labels,
			'supports'            => $support_editors,
			//'taxonomies'          => array( 'category', 'post_tag' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			//'menu_icon'           => '',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		register_post_type( 'model', $args );
	}

	// Register Custom Post Category
	function thg_register_model_post_type_category()  {

		$labels = array(
			'name'                       => _x( 'Model Categories', 'Taxonomy General Name', 'thg' ),
			'singular_name'              => _x( 'Model Category', 'Taxonomy Singular Name', 'thg' ),
			'menu_name'                  => __( 'Model Category', 'thg' ),
			'all_items'                  => __( 'All Model Categories', 'thg' ),
			'parent_item'                => __( 'Parent Model Category', 'thg' ),
			'parent_item_colon'          => __( 'Parent Model Category:', 'thg' ),
			'new_item_name'              => __( 'New Model Category Type', 'thg' ),
			'add_new_item'               => __( 'Add New Model Category', 'thg' ),
			'edit_item'                  => __( 'Edit Model Category', 'thg' ),
			'update_item'                => __( 'Update Model Category', 'thg' ),
			'separate_items_with_commas' => __( 'Separate model categories with commas', 'thg' ),
			'search_items'               => __( 'Search model categories', 'thg' ),
			'add_or_remove_items'        => __( 'Add or remove model categories', 'thg' ),
			'choose_from_most_used'      => __( 'Choose from the most used model categories', 'thg' ),
		);

		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);

		register_taxonomy( 'model-category', 'model', $args );
	}
} // END class

// Add our model post type
add_action('init',array('thg_model_custom_post_type_class','thg_register_model_post_type'));
add_action('init',array('thg_model_custom_post_type_class','thg_register_model_post_type_category'));

?>