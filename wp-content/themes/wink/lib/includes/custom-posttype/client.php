<?php
/*
 *Program Name : Custom Post Type
 */

class thg_client_custom_post_type_class {
	
	// Register Custom Post Type
	function thg_register_client_post_type() {

		$support_editors = array(
			'title', 
			'thumbnail'
		);

		$labels = array(
			'name'                => _x( 'Client', 'Post Type General Name', 'thg' ),
			'singular_name'       => _x( 'Client', 'Post Type Singular Name', 'thg' ),
			'menu_name'           => __( 'Client', 'thg' ),
			'parent_item_colon'   => __( 'Parent Client:', 'thg' ),
			'all_items'           => __( 'All Clients', 'thg' ),
			'view_item'           => __( 'View Client', 'thg' ),
			'add_new_item'        => __( 'Add New Client', 'thg' ),
			'add_new'             => __( 'New Client', 'thg' ),
			'edit_item'           => __( 'Edit Client', 'thg' ),
			'update_item'         => __( 'Update Client', 'thg' ),
			'search_items'        => __( 'Search clients', 'thg' ),
			'not_found'           => __( 'No clients found', 'thg' ),
			'not_found_in_trash'  => __( 'No clients found in Trash', 'thg' ),
		);

		$args = array(
			'label'               => __( 'client', 'thg' ),
			'description'         => __( 'Client information pages', 'thg' ),
			'labels'              => $labels,
			'supports'            => $support_editors,
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		register_post_type( 'client', $args );
	}

} // END class

// Add our client post type
add_action('init',array('thg_client_custom_post_type_class','thg_register_client_post_type'));

?>