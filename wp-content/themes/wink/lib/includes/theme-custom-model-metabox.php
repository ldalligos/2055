<?php
/*
 *Program Name : Custom Page Metaboxes
*/

// Metaboxes for contact us page template
add_action( 'add_meta_boxes', 'add_models_meta_box' );
add_action( 'save_post', 'dynamic_save_models_meta_box' );

function add_models_meta_box() {
    global $post;
    $models = '';

    if (get_post_meta( $post->ID, '_wp_page_template', true ) != "") {
        if ( preg_match("/$models.php/i", get_post_meta( $post->ID, '_wp_page_template', true )) ) {
            add_meta_box('dynamic_sectionid', __( 'Wink Theme Custom Settings', 'myplugin_textdomain' ), 'models_meta_box', 'page');
        }
    }
}

function models_meta_box() {
    global $post;
    wp_nonce_field( plugin_basename( __FILE__ ), 'dynamicMeta_noncename' );
    
    $model_category = get_post_meta( $post->ID, 'model_category', true );
    ?>
    
    <table class="woo_metaboxes_table">
        <tbody>
            <tr class="woo-custom-field woo-field-type-text">        
                <th class="woo_metabox_names"><label class="custom-meta-label" for="contactForm7">Model Category</label></th>
               
                <td>

                     
                    <select name='model_category' id='model_category' class=''>
                         <option value="select-model"> Select Model Category</option>
                        <?php 
                            $categories = get_categories('title_li=&orderby=name&hide_empty=0&taxonomy=model-category');
                            foreach($categories as $category){
                        ?>    
                            <option value="<?php echo $category->slug; ?>" <?php selected( $category->slug, $model_category ); ?>> <?php echo $category->name; ?> </option>
                               
                        <?php  }  ?>
                    

                    </select>
            </tr>
        </tbody>
    </table>
    <?php 
}

function dynamic_save_models_meta_box( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
        return;
    if ( !isset( $_POST['dynamicMeta_noncename'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename'], plugin_basename( __FILE__ ) ) )
        return;

    update_post_meta( $post_id, 'model_category', $_POST['model_category'] );
}

?>