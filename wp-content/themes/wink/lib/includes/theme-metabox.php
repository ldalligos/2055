<?php

	global $post;

	$woo_metaboxes = array();

	/* START NICK */
	if ( 'our-people' == get_post_type() || !get_post_type() ) {

			// This is the content of the "Custom Settings"
			// If you want to remove the "Custom Settings" just remove this snippets.
			
			$woo_metaboxes["_position"] = array (
				"name"  => "position",
				"std"  => "",
				"label" => "Position",
				"type" => "text",
				"desc" => "Enter the position here."
			);
			
			$woo_metaboxes["_email"] = array (
				"name"  => "email",
				"std"  => "",
				"label" => "Email",
				"type" => "text",
				"desc" => "Enter the email here."
			);
			
			$woo_metaboxes["_contact_number"] = array (
				"name"  => "contact_number",
				"std"  => "",
				"label" => "Contact Number",
				"type" => "text",
				"desc" => "Enter the contact number here."
			);
	}
	/* END NICK */
	if ( 'model' == get_post_type() || !get_post_type() ) {

		// This is the content of the "Custom Settings"
		// If you want to remove the "Custom Settings" just remove this snippets.
		
		$woo_metaboxes["_model_availability"] = array (
			"name"  => "model_availability",
			"std"  => "",
			"label" => "Is the model available?",
			"type" => "radio",
			"desc" => "Is the model available",
			"options" => array("Yes" => "Yes", "No" => "No")
		);
		
		$woo_metaboxes["_model_location"] = array (
			"name"  => "model_location",
			"std"  => "",
			"label" => "Location: ",
			"type" => "text",
			"desc" => "Enter the location of the model."
		);

		$woo_metaboxes["_model_height"] = array (
			"name"  => "model_height",
			"std"  => "",
			"label" => "Height: ",
			"type" => "text",
			"desc" => "Enter the height of the model."
		);

		$woo_metaboxes["_model_eyes"] = array (
			"name"  => "model_eyes",
			"std"  => "",
			"label" => "Eye Color: ",
			"type" => "text",
			"desc" => "Enter the eye color of the model."
		);

		$woo_metaboxes["_model_hair"] = array (
			"name"  => "model_hair",
			"std"  => "",
			"label" => "Hair Color: ",
			"type" => "text",
			"desc" => "Enter the hair color of the model."
		);

		$woo_metaboxes["_model_shoe"] = array (
			"name"  => "model_shoe",
			"std"  => "",
			"label" => "Shoe Size: ",
			"type" => "text",
			"desc" => "Enter the shoe size of the model."
		);

		$woo_metaboxes["_model_size"] = array (
			"name"  => "model_size",
			"std"  => "",
			"label" => "Body Built Size: ",
			"type" => "text",
			"desc" => "Enter the body built size of the model."
		);

		/*$woo_metaboxes["_sample_text"] = array (
			"name"  => "sample_text",
			"std"  => "",
			"label" => "Sample Text",
			"type" => "text",
			"desc" => "This is the description."
		);
		
		$woo_metaboxes["_sample_upload"] = array (
			"name"  => "sample_upload",
			"std"  => "",
			"label" => "Sample Upload",
			"type" => "upload",
			"desc" => "This is the description."
		);
		
		$woo_metaboxes["_sample_textarea"] = array (
			"name"  => "sample_textarea",
			"std"  => "",
			"label" => "Sample Textarea",
			"type" => "textarea",
			"desc" => "This is the description."
		);
		
		$woo_metaboxes["_sample_calendar"] = array (
			"name"  => "sample_calendar",
			"std"  => "",
			"label" => "Sample Calendar",
			"type" => "calendar",
			"desc" => "This is the description."
		);
		
		$woo_metaboxes["_sample_text"] = array (
			"name"  => "sample_text",
			"std"  => "",
			"label" => "Sample Text",
			"type" => "text",
			"desc" => "This is the description."
		);
		
		$woo_metaboxes["_sample_time"] = array (
			"name"  => "sample_time",
			"std"  => "",
			"label" => "Sample Time",
			"type" => "time",
			"desc" => "This is the description."
		);
		
		$woo_metaboxes["_sample_time_mask"] = array (
			"name"  => "sample_time_mask",
			"std"  => "",
			"label" => "Sample Time Mask",
			"type" => "time_masked",
			"desc" => "This is the description."
		);
		
		$woo_metaboxes["_sample_select"] = array (
			"name"  => "sample_select",
			"std"  => "",
			"label" => "Sample Select",
			"type" => "select2",
			"desc" => "This is the description.",
			"options" => array(
							"Option1" => "Option 1", "Option2" => "Option 2"
						)
		);
		
		$woo_metaboxes["_sample_check"] = array (
			"name"  => "sample_check",
			"std"  => "",
			"label" => "Sample Checkbox",
			"type" => "checkbox",
			"desc" => "This is the description. True or False"
		);
		
		$woo_metaboxes["_sample_radio"] = array (
			"name"  => "sample_radio",
			"std"  => "",
			"label" => "Sample Radio",
			"type" => "radio",
			"desc" => "This is the description. True or False",
			"options" => array("Option1" => "Option1", "Option2" => "Option2")
		);
		
		$woo_metaboxes["_sample_images"] = array (
			"name"  => "sample_images",
			"std"  => "",
			"label" => "Sample Images",
			"type" => "images",
			"desc" => "Choose among these images",
			"options" => array(
							"Option1" => $theme_url."/functions/images/1c.png",
							"Option2" => $theme_url."/functions/images/2cl.png",
							"Option3" => $theme_url."/functions/images/2cr.png",
							"Option4" => $theme_url."/functions/images/3cl.png",
							"Option5" => $theme_url."/functions/images/3cm.png",
							"Option6" => $theme_url."/functions/images/3cr.png",
						)
		);*/

	}

?>