<?php
/*
 *Program Name : Custom Page Metaboxes
*/

// Metaboxes for why wink page template
add_action( 'add_meta_boxes', 'add_why_wink_meta_box' );
add_action( 'save_post', 'dynamic_save_why_wink_meta_box' );

function add_why_wink_meta_box() {
    global $post;
    if (get_post_meta( $post->ID, '_wp_page_template', true ) != "") {
        if ( preg_match('/$why-wink.php/i', get_post_meta( $post->ID, '_wp_page_template', true )) ) {
            add_meta_box('dynamic_sectionid', __( 'Wink Theme Custom Settings', 'myplugin_textdomain' ), 'why_wink_meta_box', 'page');
        }
    }
}

function why_wink_meta_box() {
    global $post;
    wp_nonce_field( plugin_basename( __FILE__ ), 'dynamicMeta_noncename' );
    $ourServices = get_post_meta( $post->ID, 'ourServices', true );
    ?>
    <table class="woo_metaboxes_table">
        <tbody>
            <tr class="woo-custom-field woo-field-type-text">        
                <th class="woo_metabox_names"><label class="custom-meta-label" for="ourServices">Our Services Content:</label></th>
                <td>
                    <div class="customEditor"><textarea name="ourServices"><?php echo wp_richedit_pre($ourServices); ?></textarea></div>
                    <span class="woo_metabox_desc">Enter the content of our services here.</span>
                </td>
            </tr>
        </tbody>
    </table>
    <?php 
}

function dynamic_save_why_wink_meta_box( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
        return;
    if ( !isset( $_POST['dynamicMeta_noncename'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename'], plugin_basename( __FILE__ ) ) )
        return;

    update_post_meta( $post_id, 'ourServices', $_POST['ourServices'] );
}

// important: note the priority of 99, the js needs to be placed after tinymce loads
add_action('admin_print_footer_scripts','my_admin_print_footer_scripts',99);
function my_admin_print_footer_scripts()
{
    ?><script type="text/javascript">/* <![CDATA[ */
        jQuery(function($)
        {
            var i=1;
            $('.customEditor textarea').each(function(e)
            {
                var id = $(this).attr('id');
 
                if (!id)
                {
                    id = 'customEditor-' + i++;
                    $(this).attr('id',id);
                }
 
                tinyMCE.execCommand('mceAddControl', false, id);
                 
            });
        });
    /* ]]> */</script><?php
}
?>