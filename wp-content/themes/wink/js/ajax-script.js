jQuery(document).ready( function() {

   jQuery(".add-model").live('click', function(e) {
      e.preventDefault();
      var post_id = jQuery(this).attr("data-post_id");
      var nonce = jQuery('#ajax-nonce').val();
      var datas = {action: "addmodel", post_id : post_id, nonce: nonce};

      console.log(nonce);
      jQuery.ajax({
         type : "post",
         url : myAjax.ajaxurl,
         data : datas,
         success: function(response) {
            jQuery('#my-lightbox .count').html('(' + response + ')');
         },

         error: function(response){
            //console.log(response.type);

         }
      })



   });


   jQuery(".remove-model").live('click', function(e) {
      e.preventDefault();
      var post_id = jQuery(this).attr("data-post_id");
      var nonce = jQuery('#ajax-nonce').val();
      var datas = {action: "removemodel", post_id : post_id, nonce: nonce};
      var targetLi =  post_id;
      //console.log(nonce);
      jQuery.ajax({
         type : "post",
         url : myAjax.ajaxurl,
         data : datas,
         success: function(response) {
            jQuery('#my-lightbox .count').html('(' + response + ')');
            jQuery('#'+targetLi).stop().fadeOut();
         },

         error: function(response){
            //console.log(response.type);

         }
      })   

   });

});