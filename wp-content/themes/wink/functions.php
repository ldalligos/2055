<?php

add_action('after_setup_theme', 'thg_framework_setup');
if ( ! isset( $content_width ) ) $content_width = 1140;

function thg_framework_setup(){
	
	global $settings;
	
	// SHOW_WOO_PAGINATION | Value = True or False | If false, you will use WordPress Default Pagination
	// SHOW_POST_NAVIGATION | Value = True or False | If true, you will be able to see the "Next and Previous" post link
	// SHOW_BREADCRUMB | Value = True or False | If false, breadcrumb will not appear
	// SHOW_POST_META_HEADER | Value = True or False | If true, the "author" and "date publish" will be displayed
	// SHOW_POST_META_FOOTER  Value = True or False | If true, the "category" and "tag" will be displayed
	
	$settings = array(
		'show_woo_pagination' => true,
		'show_post_navigation' => true,
		'show_breadcrumb' => true,
		'show_post_meta_header' => true,
		'show_post_meta_footer' => true,
		'support_background' => true,
		'support_post_format' => false
	);
	
	include_once('lib/includes/theme-custom-posttype.php');		// Use this to create custom post types
	include_once('lib/includes/theme-sidebar.php');						// Use this to create/register sidebars
	include_once('lib/includes/theme-widget.php');						// Use this to create custom widgets
	include_once('lib/includes/theme-helper.php');						// Contains built-in functions by HTML Guys
	include_once('lib/includes/theme-setup.php');							// Contains theme setup.settings
	include_once('lib/includes/theme-options.php');						// Use this to create Theme Options and Post Metaboxes
	include_once('lib/includes/theme-custom-functions.php');	// Use this to create your own functions
	include_once('functions/admin-init.php');							    // Loads Woo Framework

  // Vendor Functions/Helpers
  require_once('lib/aq_resizer.php');                       // Resize images on the fly

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1500, 9999 ); // Unlimited height, soft crop

	if( true == $settings['support_background'] ){
		add_theme_support( 'custom-background' );
	}
	
	if( true == $settings['support_post_format'] ){
		add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat') );
	}
	
}




/* AJAX SCRIPT */
add_action("wp_ajax_addmodel", "add_model_to_lightbox");
add_action("wp_ajax_nopriv_addmodel", "add_model_to_lightbox");

function add_model_to_lightbox() {

   //print_r($_REQUEST);	

   if ( !wp_verify_nonce( $_REQUEST['nonce'], "my_user_vote_nonce")) {
      exit("No naughty business please");
   }   


   if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //$result = json_encode($result);
      //echo $result;

   		if( !session_id())
  			session_start();


  		$lightbox[] = $_REQUEST['post_id'];
  		$temp_lightbox = $id = '';

  		if(isset($_SESSION['lightbox_data'])){
  			
  			$id = $_SESSION['lightbox_data'];

  			//print_r($id);

  			if(!in_array($_REQUEST['post_id'], $id)){
  				$temp_lightbox = $_SESSION['lightbox_data'];
  				array_push($temp_lightbox, $_REQUEST['post_id']);
  				$_SESSION['lightbox_data'] = $temp_lightbox;

  				

  			}else{
  				$_SESSION['lightbox_data'] = $_SESSION['lightbox_data'];

  			}

  			

  		}else{
  			$_SESSION['lightbox_data'] = $lightbox;
  		}


  		
  		echo count($_SESSION['lightbox_data']);

   }
   else {
      header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}




add_action("wp_ajax_removemodel", "remove_model_to_lightbox");
add_action("wp_ajax_nopriv_removemodel", "remove_model_to_lightbox");

function remove_model_to_lightbox() {

   //print_r($_REQUEST);	

   if ( !wp_verify_nonce( $_REQUEST['nonce'], "my_user_vote_nonce")) {
      exit("No naughty business please");
   }   


   if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //$result = json_encode($result);
      //echo $result;

   		if( !session_id())
  			session_start();

  		if(isset($_SESSION['lightbox_data'])){
  			$new_ids = array_remove($_SESSION['lightbox_data'], $_REQUEST['post_id']);
  			$_SESSION['lightbox_data'] = $new_ids;
  			//print_r($_SESSION['lightbox_data']);
  		}	
  		
  		echo count($_SESSION['lightbox_data']);

   }
   else {
      header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}







function array_remove($arr,$value) { 
   return array_values(array_diff($arr,array($value))); 
} 


function my_must_login() {
   echo "You must log in to vote";
   die();
}



add_action( 'init', 'ajax_required_script' );

function ajax_required_script() {
   wp_register_script( "add_model_script", get_template_directory_uri().'/js/ajax-script.js', array('jquery') );
   wp_localize_script( 'add_model_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        

   wp_enqueue_script( 'jquery' );
   wp_enqueue_script( 'add_model_script' );

}



function register_session(){
    if( !session_id())
        session_start();
}

add_action('init','register_session');

/* START NICK */
include("lib/includes/theme-custom-contact-us-metabox.php");
include("lib/includes/theme-custom-why-wink-metabox.php");
include("lib/includes/theme-custom-model-metabox.php");
/* END NICK */