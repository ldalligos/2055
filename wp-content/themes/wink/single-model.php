<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress Kickstarter Theme
 */

//If the form is submitted
if( isset( $_POST['submitted'] ) ) {
	$response = array("status" => false);
	//Check to make sure that the recipient_name field is not empty
	if( trim( $_POST['recipient_name'] ) === '' ) {
		$response["recipientNameError"] =  __( 'You forgot to enter the recipient name.', 'woothemes' );
		$hasError = true;
	} else {
		$recipient_name = trim( $_POST['recipient_name'] );
	}
	
	//Check to make sure that the name field is not empty
	if( trim( $_POST['sender_name'] ) === '' ) {
		$response["senderNameError"] =  __( 'You forgot to enter the sender name.', 'woothemes' );
		$hasError = true;
	} else {
		$sender_name = trim( $_POST['sender_name'] );
	}

	//Check to make sure sure that a valid recipient email address is submitted
	if( trim( $_POST['recipient_email'] ) === '' )  {
		$response["recipientEmailError"] = __( 'You forgot to enter the recipient email address.', 'woothemes' );
		$hasError = true;
	} else if ( ! eregi( "^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim( $_POST['recipient_email'] ) ) ) {
			$response["recipientEmailError"] = __( 'You entered an invalid recipient email address.', 'woothemes' );
			$hasError = true;
		} else {
		$recipient_email = trim( $_POST['recipient_email'] );
	}
	//Check to make sure sure that a valid sender email address is submitted
	if( trim( $_POST['sender_email'] ) === '' )  {
		$response["senderEmailError"] = __( 'You forgot to enter the sender email address.', 'woothemes' );
		$hasError = true;
	} else if ( ! eregi( "^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim( $_POST['sender_email'] ) ) ) {
			$response["senderEmailError"] = __( 'You entered an invalid sender email address.', 'woothemes' );
			$hasError = true;
		} else {
		$sender_email = trim( $_POST['sender_email'] );
	}

	$message = stripslashes( trim( $_POST['message'] ) );

	//If there is no error, send the email
	if( ! isset( $hasError ) ) {
		$emailTo = $recipient_email;
		$subject = $sender_name . __( ' shared a link from Wink models.', 'woothemes' );
		$body = $message."\n\r\n\r wink model link: ".$_POST['url'];
		$headers = __( 'From: ', 'woothemes' ) . "$sender_name <$sender_email>";

		wp_mail( $emailTo, $subject, $body, $headers );

		$response["status"] = true;

	}
	echo json_encode( $response );
	exit();
}

get_header();

?>
	<div class="container">
	<section class="primary">
		<div class="entry" role="main">
			<?php
				$imgs_args = array(
					'numberposts'   => -1,
			        'order'         => 'ASC',
			        'post_mime_type'=> 'image',
			        'post_type'     => 'attachment',
			        'post_parent'   => $post->ID,
			        'exclude' => get_post_thumbnail_id()
					);
				$imgs = get_children( $imgs_args );
			?>

			<?php if( have_posts() ): ?>
				
				<div class="breadcrumbs">
			    <?php if(function_exists('bcn_display'))
			    {
			      bcn_display();
			    }?>
			  </div>

				<?php while( have_posts() ): the_post(); ?>	

				<?php 
					$location = get_post_meta($post->ID, 'model_location', true);
					$height = get_post_meta($post->ID, 'model_height', true);
					$eyes = get_post_meta($post->ID, 'model_eyes', true);
					$hair = get_post_meta($post->ID, 'model_hair', true);
					$shoe = get_post_meta($post->ID, 'model_shoe', true);
					$size = get_post_meta($post->ID, 'model_size', true);
				?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( get_post_format(), get_the_ID() ); ?>>
						<div class="head">
							<h1 class="model-name"><?php the_title(); ?></h1>
							<div class="item-buttons">
								<a class="add-btn btn add-model" href="#" data-post_id="<?php echo $post->ID;?>"><span class="icon icon-heart"></span>Add</a>
								<a class="share-btn btn" href="#"><span class="icon icon-mail"></span>Share</a>
   							<a class="comp-btn btn" href="#"><span class="icon icon-user"></span>Comp</a>
							</div>
						</div>

						<section class="share">
							<form class="" id="share" action="<?php the_permalink(); ?>" method="post">
								<h3>Share this model with a friend</h3>
								<div class="col-1">
									<input type="text" id="txt_senderName" placeholder="Your Name *" class="requiredField" >
									<input type="text" id="txt_senderEmail" placeholder="Your Email *" class="requiredField email" >
									<input type="text" id="txt_recipientName" placeholder="Recipient Name *" class="requiredField" >
									<input type="text" id="txt_recipientEmail" placeholder="Recipient Email *" class="requiredField email" >
								</div>
								<div class="col-2">
									<textarea id="txt_message" placeholder="Your Message"></textarea>
								</div>
								<div class="message"></div> <input type="submit" value="SUBMIT" name="btn_submit" id="btn_submit" />
								<div class="clearfix"></div>
							</form>
						</section>

						<div class="gallery">
							<ul class="model-layout">
							<?php if( $imgs ) { 

								$model_images = array();
								$blank = array(2,6,8,12,14,16,18,20,21,23,25);

								foreach( $imgs as $img ) { 
									$model_images[] = $img;
								}
								
								$total_images = count($model_images);	
								
								if($total_images == 1){ $gtotal = 3;}	
								if($total_images == 2){ $gtotal = 4;}
								if($total_images == 3){ $gtotal = 5;}
								if($total_images == 4){ $gtotal = 7;}
								if($total_images == 5){ $gtotal = 9;}
								if($total_images == 6){ $gtotal = 10;}
								if($total_images == 7){ $gtotal = 11;}
								if($total_images == 8){ $gtotal = 13;}
								if($total_images == 9){ $gtotal = 15;}								
								if($total_images == 10){ $gtotal = 16;}
								if($total_images == 11){ $gtotal = 18;}
								if($total_images == 12){ $gtotal = 20;}	
								if($total_images == 13){ $gtotal = 21;}	
								if($total_images == 14){ $gtotal = 23;}	
								if($total_images == 15){ $gtotal = 25;}	
								if($total_images == 16){ $gtotal = 26;}	
								if($total_images == 17){ $gtotal = 28;}	
								if($total_images == 18){ $gtotal = 30;}	
								if($total_images == 19){ $gtotal = 21;}	
								if($total_images == 20){ $gtotal = 33;}			


								$data = array();
								$co = 0;
								for($x = 1; $x <= $gtotal; $x++){

									if(in_array($x, $blank)){
										$y = array('type' => 'blank', 'value' =>'blank');
										array_push($data, $y);
									
									}elseif($x == 5){
										$d = array('type' => 'details', 'value' =>'details');
										array_push($data, $d);	
									}
									elseif ( $x == 3 && $gtotal == 3) {
										$d = array('type' => 'details', 'value' =>'details');
										array_push($data, $d);
									}
									else{
										$f = array('type' => 'id', 'value' => $model_images[$co]);
										array_push($data, $f);
										$co++;
									}
								}


								//print_r($data);
								?>
									

									
									<?php /* Setting Sizes */
									
									$total_data = count($data);

									for($v = 0; $v < $total_data; $v++){

										if($data[$v]['type'] == 'id'){ 
											$image_data = $data[$v]['value'];	
											$preview_size = wp_get_attachment_image_src( $image_data->ID, $size='medium' );
											
											$resized = $preview_size[0];

											$preview_size_thumb = aq_resize($resized, 187, 190, true);

									    $full_size = wp_get_attachment_image_src( $image_data->ID, $size='full-size' ); ?>
									        
									        <li class="cs-item">
									        	<a href="#lightbox_gallery" class="fancybox" alt="<?php echo $image_data->post_title; ?>">
									            	<img src="<?php echo $preview_size_thumb; ?>" alt="<?php echo $image_data->post_title; ?>" />
									        	</a>
									    	</li>
										
										<?php }
										if($data[$v]['type'] == 'details'){ ?>
											<li> 
												<ul class="model-details">
													<li><span class="label">Location:</span><p class="value"><?php echo $location?></p></li>
													<li><span class="label">Height:</span><p class="value"><?php echo $height?></p></li>
													<li><span class="label">Eyes:</span><p class="value"><?php echo $eyes?></p></li>
													<li><span class="label">Hair:</span><p class="value"><?php echo $hair?></p></li>
													<li><span class="label">Shoe:</span><p class="value"><?php echo $shoe?></p></li>
													<li><span class="label">Size:</span><p class="value"><?php echo $shoe?></p></li>
												</ul>	
											</li>
										<?php } ?>

										<?php if($data[$v]['type'] == 'blank'){ ?>

											<li class="cs-item"> </li>

										<?php } ?>

											

									<?php } ?>
								
							        
							    
							    
							  <?php } ?>
						  </ul> 
						</div>						
					</article><?php
					endwhile; 

				?>
			<!-- Ajax hiddend fields -->
		  <?php
		   $nonce = wp_create_nonce("my_user_vote_nonce");
		   $link = admin_url('admin-ajax.php?action=add_model_to_lightbox&post_id='.$post->ID.'&nonce='.$nonce);
		  
		    //echo '<a class="add-btn btn add-model" data-nonce="' . $nonce . '" data-post_id="' . $post->ID . '" href="' . $link . '"><span class="icon icon-heart"></span>Add</a>';
		  ?>
			<div id="lightbox_gallery">
		  		  <div id="slider" class="flexslider"> 
					<ul class="slides">
						<?php if( $imgs ) : ?>
							<?php foreach( $imgs as $img ) : ?>
								<?php /* Setting Sizes */
					        $full_size = wp_get_attachment_image_src( $img->ID, $size='full-size' ); ?>
					        <li>
				        	    <img src="<?php echo $full_size[0]; ?>" alt="<?php echo $img->post_title; ?>" />
						    </li>
					    	<?php endforeach; ?>
					  	<?php endif; ?>
				  	</ul> 
			  	</div>
		  		<div id="carousel" class="flexslider"> 
					<ul class="slides">
						<?php if( $imgs ) : ?>
							<?php foreach( $imgs as $img ) : ?>
								<?php /* Setting Sizes */
					        $preview_size = wp_get_attachment_image_src( $img->ID, $size='medium' ); 

					        $resized2 = $preview_size[0];

									$gallery_thumbnails = aq_resize($resized2, 116, 116, true);
					       ?>
					        <li>
			        	    <img src="<?php echo $gallery_thumbnails; ?>" alt="<?php echo $img->post_title; ?>" />
						    	</li>
					    	<?php endforeach; ?>
					  	<?php endif; ?>
				  	</ul> 
				</div> 
			</div>
		  <input type="hidden" name="ajax-nonce" value="<?php echo $nonce;?>" id="ajax-nonce" />
			<?php else: ?>
				
				<?php get_template_part( 'templates/partials/inc', 'noresult' ); ?>
				
			<?php endif; ?>
		</div><!-- end entry -->
	</section><!-- end primary -->

	<?php //get_sidebar(); ?>
	</div>


<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function($){
	$(document).ready(function() {
		$("form#share").submit(function(e) {
			e.preventDefault();
			var hasError = false;
			$( '.requiredField', this).each(function() {
				if($.trim($(this).val()) == '') {
					$(this).addClass( 'inputError' );
					hasError = true;
				} else if($(this).hasClass( 'email')) {
					var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					if(!emailReg.test($.trim($(this).val()))) {
						$(this).addClass( 'inputError' );
						hasError = true;
					}
				}
			});
			if(!hasError) {
				$(".share .message").text("Loading... Please wait.").addClass("loading").removeClass("error");
				$.ajax({
				    url : $(this).attr("action"),
				    type: "POST",
				    data : {
				    	sender_name: $("#txt_senderName").val(),
				    	sender_email: $("#txt_senderEmail").val(),
				    	recipient_name: $("#txt_recipientName").val(),
				    	recipient_email: $("#txt_recipientEmail").val(),
				    	message: $("#txt_message").val(),
				    	url: location.href,
				    	submitted: true
				    },
				    dataType : "json",
				    success: function(data, textStatus, jqXHR) {
				    	if(data["status"]) {
				    		$(".share .message").text("You have successfully shared the model.").addClass("success").removeClass("error loading");
				    	}
				    },
				    error: function (jqXHR, textStatus, errorThrown) {
				    	$(".share .message").text("Can not send your email. Something went wrong. Sorry.").addClass("error").removeClass("loading success");
				 		console.log("Can not send your email. Something went wrong. Sorry.")
				    }
				});
			} else {
				$(".share .message").text("You must fill in the items highlighted in red to proceed.").addClass("error").removeClass("loading success");
			}
		});

	});
})(jQuery)
//-->!]]>
</script>

<?php get_footer(); ?>