<?php
/*
Template Name: Case Study Template
*/

get_header(); 

global $woo_options; ?>
	<div class="container">
	<section class="primary">
		<div class="entry" role="main">
			<?php if( have_posts() ): while( have_posts() ): the_post();				

			//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	        
	        //$number_of_case_study_to_display = $woo_options['woohg_no_case_studies'];

			$args = array(
	          'post_type' => 'case_study',
	          //'posts_per_page' => $number_of_case_study_to_display, 
	          'post_status' => 'publish',
	          'order' => 'DESC', 
	          'orderby' => 'date'
	          //'paged' => $paged
	        );

	        $case_studies_query = new WP_Query( $args );

        	require( locate_template( 'templates/partials/loop-case-study.php' ) );
        
        	// Display page content
        	the_content();
     		?>


      		<?php endwhile; ?>
				
			<?php else: ?>
				<?php get_template_part( 'templates/partials/inc', 'noresult' ); ?>
			<?php endif; ?>
		</div><!-- END .entry -->
	</section><!-- END .primary -->

		<?php //get_sidebar(); ?>
	</div>


<?php get_footer(); ?>