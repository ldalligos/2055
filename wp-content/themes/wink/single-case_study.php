<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress Kickstarter Theme
 */

get_header(); ?>
	<div class="container">
	<section class="primary">
		<div class="entry" role="main">
			<?php if( have_posts() ): ?>
				
				<div class="breadcrumbs">
			    <?php if(function_exists('bcn_display'))
			    {
			      bcn_display();
			    }?>
			  </div>

				<?php while( have_posts() ): the_post(); ?>	

					
					<ul class="model-layout">
  
					    <!-- The Loop -->
					    
					    <?php 
					    	$case_study_title = get_the_title();
					     	if ( $post->post_type == 'case_study' && $post->post_status == 'publish' ) {
							$attachments = get_posts( array(
								'post_type' => 'attachment',
								'posts_per_page' => -1,
								'post_parent' => $post->ID,
								'exclude'     => get_post_thumbnail_id()
							) );

							$counter = 1;
							$hint = array(2,4,6,8,10,12,14,16,18,20,22,24,26,28,30);

							if ( $attachments ) {
								foreach ( $attachments as $attachment ) {
									$cs_images[] = array('id' => $attachment);
								}

								$total_cs_images = count($cs_images);
								$given_param = 2;
								if($total_cs_images > 3){
									$over_all_count = $total_cs_images * $given_param; 
								}else{
									$over_all_count = $total_cs_images * $given_param;
								}

								$h = 1;
								for($x = 1; $x <= $over_all_count; $x++){
									
									if(in_array($x, $hint)){
										$t[$x] = array('content' => 'blank', 'type' =>'blank');
									}else{

										if($x == 1){
											$t[$x] = array('content' => $cs_images[0]['id'], 'type' =>'image');
										}else{
											

											$t[$x] = array('content' => $cs_images[$h]['id'], 'type' =>'image');
											$h++;
										}
										
									}
								}	
									
								


								$loop_images_count = count($t);
								for($q = 1; $q <= $loop_images_count; $q++){

									$class = "post-attachment";
									
									if($q == 2){
										echo '<li class="' . $class . ' cs-item has-title"><h2 class="cs-title">'.$case_study_title.'</h2></li>';
									}

									elseif($q == 4){
										echo '<li class="' . $class . ' cs-item has-content">'.get_the_content().'</li>';
									}else{

										if($t[$q]['content'] !== 'blank'){
											$thumbimg = wp_get_attachment_image( $t[$q]['content']->ID, 'thumbnail-size', true );
											echo '<li class="' . $class . ' cs-item">'.$thumbimg.'</li>';
										}

										if($t[$q]['content'] === 'blank'){
											echo '<li class="' . $class . ' cs-item"></li>';
										}

									}


									
									

								}

									
									
									
			

								
							}		
						}
					    ?>


					</ul>
					<!-- END of the loop -->

					<div class="clearfix"></div>

					<?php
					endwhile; 
				?>

			<?php else: ?>
				
				<?php get_template_part( 'templates/partials/inc', 'noresult' ); ?>
				
			<?php endif; ?>
		</div><!-- end entry -->
	</section><!-- end primary -->

	<?php //get_sidebar(); ?>
	</div>
<?php get_footer(); ?>