(function($) {

	//Lightbox + Gallery
	$(".fancybox").fancybox({
		padding: 0,
		width: "90%",
		maxHeight: "95%",
		height : '90%',
   		autoSize : false,
   		openEffect : "elastic",
   		closeEffect : "elastic",
   		scrolling: "no",
   		helpers:  {
	        title:  null
	    },
	    beforeShow: function() {
	    	$("#lightbox_gallery").css("position", "relative");
	    },
	    afterShow: function() {
	    	$("#lightbox_gallery").css("visibility", "visible");
	    	$(".fancybox-inner").css("overflow-x", "hidden");
	    	$("#slider").data("flexslider").resize();
	    	$("#carousel").data("flexslider").resize();
	    },
	    onUpdate : function() {
	    	jQuery(window).resize();
	    	jQuery("#slider").data("flexslider").resize();
	    }
	});
	$('#carousel').flexslider({
	    animation: "slide",
	    controlNav: false,
	    slideshow: false,
	    itemWidth: 116,
	    asNavFor: '#slider'
	});
	   
	$('#slider').flexslider({
	    animation: "slide",
	    controlNav: false,
	    slideshow: false,
	    directionNav: false,
   		smoothHeight : true,
	    sync: "#carousel",
	    start: function(slider) {
		    $('img').click(function(event){
		        event.preventDefault();
		        $('#slider').data("flexslider").flexAnimate($('#slider').data("flexslider").getTarget("next"));
		    });
		}
 	});


	$(".share-btn ").click(function(e) {
		e.preventDefault();
		$(".share").slideToggle();
	});

	$(window).resize(function() {
		if ($(this).width() > 800) {
			$(".flexslider .slides img").css("max-height", $(".fancybox-inner").height() - ($("#lightbox_gallery #carousel").height() - 10));
		} else {
			$(".flexslider .slides img").css({
				maxHeight: $(".fancybox-inner").height()
			});
		}
	});
})(jQuery);