(function($) {

	if($(window).width() < 770){
		console.log('770');
		// Mobile Menu Accordion 
		$('#menu > ul > li > a').click(function(e) {
			e.preventDefault();

		  var checkElement = $(this).next();

		  $('#menu li').removeClass('active');
		  $(this).closest('li').addClass('active'); 

		  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
		    $(this).closest('li').removeClass('active');
		    checkElement.slideUp('normal');
		  }
		  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
		    $('#menu ul ul:visible').slideUp('normal');
		    checkElement.slideDown('normal');
		  }

		  if($(this).closest('li').find('ul').children().length == 0) {
		    return true;
		  } else {
		    return false; 
		  }

		});
	}

	// Content Accordion
	$('#open-content-wrap').click(function(e){
		e.preventDefault();
		var footerHeight = $('footer.group').height();
		$(this).children('.icon').removeClass('icon-minus').addClass('icon-plus');
		if($(this).hasClass('opened')){

			$(this).css('margin-bottom',footerHeight);
			$('#home-content-wrap').slideUp(200,function(){
			$(this).closest('.home-accordion').removeAttr('style');
		});
		$(this).removeClass('opened');
		}else{
			$(this).css('margin-bottom',20).addClass('opened').closest('.home-accordion').css({'position':'fixed','bottom':footerHeight,'z-index':999}).children('#home-content-wrap').slideDown(200);
			$(this).children('.icon').addClass('icon-minus').removeClass('icon-plus');
		}
	});

	// Add class if menu has sub menu
	$('#menu > ul > li').has('.sub-menu').addClass('has-dropdown-menu');
	$('#menu > ul > li > ul > li').has('.sub-menu').addClass('has-second-level-dropdown-menu');

	// Mobile Menu Off Canvas Function
	$('#mobile-menu-trigger .mobile-menu-button').click(function(e){
		e.preventDefault();
		if(!$('body').hasClass('open-menu')){
			$('body').addClass('open-menu').children('#menu').animate({'margin-left':'0%'},300).next('#wrap').css({'position':'absolute','top':'0px'}).animate({'margin-left':'80%'},300);
		}else{
			$('body').removeClass('open-menu').children('#menu').animate({'margin-left':'-80%'},300).next('#wrap').animate({'margin-left':'0%'},300,function(){$(this).css({'position':'relative'})});
		}
	});

	// Testimonials slider
	var j = 0;
	var delay = 2000; //millisecond delay between cycles
	function cycleThru(){
		var jmax = $("#home-testimonial-list li").length -1;
		$("#home-testimonial-list li:eq(" + j + ")")
			.animate({"opacity" : "1"} ,400)
			.animate({"opacity" : "1"}, delay)
			.animate({"opacity" : "0"}, 400, function(){
				(j == jmax) ? j=0 : j++;
				cycleThru();
			});
		};

	cycleThru();

	// Share button
	$('.share-btn').click(function(e){
		if($('.share-btn').hasClass('active')) {
			$(this).removeClass('active');
		}
		else {
			$(this).addClass('active');	
		}
		
	});

})(jQuery);