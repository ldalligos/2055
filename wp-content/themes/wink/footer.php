<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress Kickstarter Theme
 */
global $woo_options; ?>

	</div><!-- END #main-content -->


<footer class="group">
	<div class="container">
		<?php get_template_part( "templates/partials/social", "media" ); ?>
		
		<div class="contact-number">
			<p class="heading">Call Us:</p>
			<span class="icon-phone icon"></span> <span class="number"><?php echo $woo_options['woohg_contact_number'] ?></span>
		</div>
		
		<ul class="colophon">
			<li><p class="copyright-text"><?php _e( 'Copyright '. date("Y") . '. ' . get_bloginfo( 'name' ), 'thg_framework' ); ?></p></li>
			<li><a href="#">Terms &amp; Conditions</a></li>
			<li><a href="www.drumbeat.net.au">Created by Drumbeat</a></li>
		</ul>  
	</div>		
</footer>
</div>
<?php wp_footer(); ?>

<script src="http://localhost:35729/livereload.js"></script>
</body>
</html>